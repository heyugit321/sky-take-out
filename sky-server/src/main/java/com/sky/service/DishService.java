package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import java.util.List;

/**
 *菜品服务接口
 */
public interface DishService {

    /**
     * 条件查询菜品和口味
     * @param dish
     * @return
     */
    List<DishVO> listWithFlavor(Dish dish);

    /**
     * 新增菜品
     * @param dishDTO 前端传的数据
     */
    void addDish(DishDTO dishDTO);

    /**
     * 分页获取菜品数据
     * @param dishPageQueryDTO
     * @return
     */
    PageResult getDishByPage(DishPageQueryDTO dishPageQueryDTO);

    /**
     * 根据ID批量删除菜品
     * @param ids
     */
    void deleteDishAll(List<Long> ids);

    /**
     * 菜品回显
     * @param id
     * @return
     */
    DishVO findDishByID(Long id);

    /**
     * 更新菜品
     * @param dishDTO
     */
    void updateDishById(DishDTO dishDTO);

    /**
     * 修改菜品状态
     * @param status
     * @param id
     */
    void StartOrStop(Integer status, Long id);

    /**
     * 根据分类ID查询菜品
     * @param categoryId
     * @return
     */
    List<Dish> getList(Long categoryId);
}
