package com.sky.service;

import com.sky.dto.OrdersSubmitDTO;
import com.sky.vo.OrderSubmitVO;

public interface OrderService {
    // 用户下单
    OrderSubmitVO submit(OrdersSubmitDTO submitDTO);
    // 更新订单状态
    void paySuccess(String orderNumber);
}
