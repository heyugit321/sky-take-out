package com.sky.service;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;

import java.util.List;

/**
 * 购物车服务
 */
public interface ShoppingCartService {
    void addShopping(ShoppingCartDTO shoppingCartDTO);
//  查看购物车列表
    List<ShoppingCart> findShoppingCart();
//  清空购物车
    void clean();
    // 删除购物车单个商品
    void deleteShoppingCart(ShoppingCartDTO shoppingCartDTO);
}
