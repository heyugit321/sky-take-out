package com.sky.service;

import com.github.pagehelper.Page;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;

import java.util.List;

/**
 * 套餐服务
 */
public interface SetmealService {

    /**
     * 条件查询
     * @param setmeal
     * @return
     */
    List<Setmeal> list(Setmeal setmeal);

    /**
     * 根据id查询菜品选项
     * @param id
     * @return
     */
    List<DishItemVO> getDishItemById(Long id);

    // 新增套餐
    void addSetmeal(SetmealDTO setmealDTO);
    // 套餐分页查询
    PageResult getSetmealByPage(SetmealPageQueryDTO pageQueryDTO);
    // 起售停售
    void startOrStop(Integer status, Long id);
    // 批量删除套餐
    void deleteSetmeal(List<Long> ids);
    // 根据ID 回显
    SetmealVO getSetmealById(Long id);
    // 更新套餐
    void updateSetmeal(SetmealDTO setmealDTO);

}
