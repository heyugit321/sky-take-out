package com.sky.service;

import com.sky.dto.UserLoginDTO;
import com.sky.vo.UserLoginVO;

/**
 * 用户服务
 */
public interface UserService {
    //用户登录
    UserLoginVO userLoginByOpenid(UserLoginDTO userLoginDTO);
}
