package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sky.constant.JwtClaimsConstant;
import com.sky.constant.MessageConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.exception.LoginFailedException;
import com.sky.mapper.UserMapper;
import com.sky.properties.JwtProperties;
import com.sky.properties.WeChatProperties;
import com.sky.service.UserService;
import com.sky.utils.HttpClientUtil;
import com.sky.utils.JwtUtil;
import com.sky.vo.UserLoginVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private WeChatProperties weChatProperties;
    @Autowired
    private JwtProperties jwtProperties;
    @Autowired
    private UserMapper userMapper;

    @Override
    public UserLoginVO userLoginByOpenid(UserLoginDTO userLoginDTO) {
        // 1.获取openid:根据临时凭证code,去微信那边获取用户的openid
        // 获取openid的条件：访问路径，准备参数，使用HttpClient
        // 2.如果openid是空，抛出业务异常
        String openId = getOpenId(userLoginDTO.getCode());
        if (openId == null){
            throw new LoginFailedException(MessageConstant.LOGIN_FAILED);
        }

        // 3.判断openid,是否在数据库存在
        // 3.1 存在，说明已登录，直接继续执行
        User user = userMapper.seleteUserByOpenId(openId);
        // 3.2 不存在，使用openid,注册一个用户
        if (user == null){
            user = new User();
            user.setOpenid(openId);
            user.setCreateTime(LocalDateTime.now());
            userMapper.insertUser(user);
        }
        // 4.生成token,封装VO对象（id,openid,token）
        Map<String,Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.USER_ID,user.getId());
        String token = JwtUtil.createJWT(jwtProperties.getUserSecretKey(), jwtProperties.getUserTtl(), claims);

        UserLoginVO userLoginVO = new UserLoginVO();

        userLoginVO.setOpenid(openId);
        userLoginVO.setId(user.getId());
        userLoginVO.setToken(token);
        return userLoginVO;
    }

    //获取openid  获取openid:根据临时凭证code,去微信那边获取用户的openid
    private String getOpenId(String code) {

        Map paramData = new HashMap();
        paramData.put("appid",weChatProperties.getAppid());
        paramData.put("secret",weChatProperties.getSecret());
        paramData.put("js_code",code);
        paramData.put("grant_type","authorization_code");

        String content = HttpClientUtil.doGet("https://api.weixin.qq.com/sns/jscode2session?", paramData);
        log.info("[content] ...........:{}",content);
        // 返回的是JSON串，需要openid；
        JSONObject jsonObject = JSON.parseObject(content);
        String openid = jsonObject.getString("openid");

        return openid;
    }
}
