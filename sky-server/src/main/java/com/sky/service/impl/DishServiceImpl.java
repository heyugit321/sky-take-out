package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class DishServiceImpl implements DishService {

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private DishFlavorMapper dishFlavorMapper;

    /**
     * 条件查询菜品和口味
     * @param dish
     * @return
     */
    public List<DishVO> listWithFlavor(Dish dish) {
        List<Dish> dishList = dishMapper.list(dish);

        List<DishVO> dishVOList = new ArrayList<>();

        for (Dish d : dishList) {
            DishVO dishVO = new DishVO();
            BeanUtils.copyProperties(d,dishVO);

            //根据菜品id查询对应的口味
            List<DishFlavor> flavors = dishFlavorMapper.getByDishId(d.getId());

            dishVO.setFlavors(flavors);
            dishVOList.add(dishVO);
        }
        return dishVOList;
    }


    /**
     * 根据分类ID查询菜品
     * @param categoryId
     * @return
     */
    @Override
    public List<Dish> getList(Long categoryId) {
        Dish dish = Dish.builder()
                .categoryId(categoryId)
                .status(StatusConstant.ENABLE)
                .build();
        return dishMapper.getList(dish);
    }

    /**
     * 菜品启用与禁用
     * @param status
     * @param id
     */
    @Override
    public void StartOrStop(Integer status, Long id) {
        Dish dish = Dish.builder()
                        .status(status)
                        .id(id)
                        .build();
        dishMapper.updateDish(dish);
    }

    /**
     * 菜品更新
     * @param dishDTO
     */
    @Transactional
    @Override
    public void updateDishById(DishDTO dishDTO) {
        // 更新菜品表
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dishMapper.updateDish(dish);
        // 更新口味表
        // 把之前的口味删掉
        dishFlavorMapper.deleteByDishId(dish.getId());
        // 再增加新的
        List<DishFlavor> flavors = dishDTO.getFlavors();

        if (flavors != null && flavors.size() > 0){
            for (DishFlavor flavor : flavors) {
                flavor.setDishId(dish.getId());
            }
            dishFlavorMapper.insertFlavors(flavors);
        }
    }

    /**
     * 菜品回显
     * @param id
     * @return
     */
    @Override
    public DishVO findDishByID(Long id) {
        // 获取菜品对象
        Dish dish = dishMapper.seleteById(id);
        // 获取口味对象
        List<DishFlavor> dishFlavorList = dishFlavorMapper.seleteByDishId(id);
        // 封装DishVO对象
         DishVO dishVO = new DishVO();
         BeanUtils.copyProperties(dish,dishVO);
        dishVO.setFlavors(dishFlavorList);
        return dishVO;
    }


    /**
     * 根据ID批量删除菜品
     * @param ids
     */
    @Transactional
    @Override
    public void deleteDishAll(List<Long> ids) {
        // 起售中的菜品不能删除
        for (Long id : ids) {
            Dish dish = dishMapper.getDishById(id);
            if (dish.getStatus() == StatusConstant.ENABLE){
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        }

        // 被套餐关联的菜品 不能删除
        Long number = dishMapper.getDishCount(ids);
        if (number != 0){
            throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }

        // 删除菜品后 关联的口味数据也删除掉
        for (Long id : ids) {
            // 删除菜品
            dishMapper.deleteDishById(id);
            // 删除口味
            dishFlavorMapper.deleteByDishId(id);
        }
    }



    /**
     * 分页查询菜品数据
     * @param dishPageQueryDTO
     * @return
     */
    @Override
    public PageResult getDishByPage(DishPageQueryDTO dishPageQueryDTO) {

        PageHelper.startPage(dishPageQueryDTO.getPage(),dishPageQueryDTO.getPageSize());

        Page<DishVO> page = (Page<DishVO>) dishMapper.selectDishByPage(dishPageQueryDTO);

        PageResult pageResult = new PageResult(page.getTotal(),page.getResult());
        return pageResult;
    }

    /**
     * 新增菜品
     * @param dishDTO 前端传的数据
     */
    @Transactional
    @Override
    public void addDish(DishDTO dishDTO) {
        // 新增菜品
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dishMapper.insertDish(dish);
        //获取insert语句生成的主键值
        Long dishId = dish.getId();
        log.info("[菜品id...]dishId:{}",dishId);

        List<DishFlavor> flavors = dishDTO.getFlavors();

        if (flavors != null && flavors.size() > 0){
            for (DishFlavor flavor : flavors) {
                flavor.setDishId(dishId);
            }
            dishFlavorMapper.insertFlavors(flavors);

        }
    }
}
