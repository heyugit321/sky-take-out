package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.ShoppingCartService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {

    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;

    /**
     * 删除购物车单个商品
     * @param shoppingCartDTO
     */
    @Override
    public void deleteShoppingCart(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO,shoppingCart);

        List<ShoppingCart> list = shoppingCartMapper.seleteByAll(shoppingCart);
        if (list != null && list.size() > 0){
            ShoppingCart shoppingcart = list.get(0);
            if (shoppingcart.getNumber() == 1){
                // 购物车只有一个 直接删除
                shoppingCartMapper.deleteCartById(shoppingcart.getId());
            }else {
                // 不只有一个
                shoppingCart.setNumber(shoppingcart.getNumber() - 1);
                shoppingCartMapper.updateShoppingById(shoppingcart);
            }
        }

    }

    /**
     * 清空购物车
     */
    @Override
    public void clean() {
         shoppingCartMapper.deleteCartByUserId(BaseContext.threadLocal.get());
    }

    /**
     * 查看购物车列表
     * @return
     */
    @Override
    public List<ShoppingCart> findShoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        // 得到用户ID
        shoppingCart.setUserId(BaseContext.getCurrentId());
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.seleteByAll(shoppingCart);
        return shoppingCartList;
    }

    /**
     * 添加购物车
     * @param shoppingCartDTO
     */
    @Override
    public void addShopping(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart selShoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO,selShoppingCart);
        // 用户ID
        selShoppingCart.setUserId(BaseContext.getCurrentId());
        // 查询数据库 有没有数据 有就数量+1 没有就插入（ 判断是菜品还是套餐）
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.seleteByAll(selShoppingCart);
        if (shoppingCartList != null && shoppingCartList.size() > 0){
            ShoppingCart shoppingCart = shoppingCartList.get(0);
            selShoppingCart.setNumber(shoppingCart.getNumber() + 1);
            //  根据这条记录的主键，去更新记录，让数量+1
            shoppingCartMapper.updateById(shoppingCart);

        }else {
            // 判断当前加入购物车的是菜品 还是套餐
            Long dishId = shoppingCartDTO.getDishId();
            if (dishId != null){
                // 说明是菜品
                Dish dish = dishMapper.getDishById(dishId);
                selShoppingCart.setName(dish.getName());
                selShoppingCart.setImage(dish.getImage());
                selShoppingCart.setAmount(dish.getPrice());
            }else {
                // 说明是套餐
                Setmeal setmeal = setmealMapper.getById(shoppingCartDTO.getSetmealId());
                selShoppingCart.setName(setmeal.getName());
                selShoppingCart.setImage(setmeal.getImage());
                selShoppingCart.setAmount(setmeal.getPrice());
            }
        }
        selShoppingCart.setNumber(1);
        selShoppingCart.setCreateTime(LocalDateTime.now());
        shoppingCartMapper.insert(selShoppingCart);
    }
}
