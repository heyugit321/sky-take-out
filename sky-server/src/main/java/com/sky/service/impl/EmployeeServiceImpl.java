package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.exception.AccountLockedException;
import com.sky.exception.AccountNotFoundException;
import com.sky.exception.PasswordErrorException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 修改员工信息
     * @param employeeDTO
     * @return
     */
    @Override
    public void updateEmp(EmployeeDTO employeeDTO) {

        Employee employee = new Employee();
        // 调用BeanUtils方法转换
        BeanUtils.copyProperties(employeeDTO,employee);
        // 修改的时间
//        employee.setUpdateTime(LocalDateTime.now());
        // 修改人是谁
//        employee.setUpdateUser(BaseContext.getCurrentId());
        employeeMapper.updateEmp(employee);

    }

    /**
     * 根据ID回显员工信息
     * @param id
     * @return
     */
    @Override
    public Employee getEmpById(Long id) {
        Employee employee = employeeMapper.getEmpById(id);
        employee.setPassword("******");
        return employee;
    }

    /**
     * 员工账号的启用与禁用
     * @param status
     * @param id
     */
    @Override
    public void startOrStop(Integer status, Long id) {

        Employee employee = Employee.builder()
                .status(status)
                .id(id)
                .build();
        employeeMapper.updateEmp(employee);

    }

    /**
     * 分页查询员工
     * @param employeePageQueryDTO
     * @return
     */
    @Override
    public PageResult seleteEmpByPage(EmployeePageQueryDTO employeePageQueryDTO) {

        PageHelper.startPage(employeePageQueryDTO.getPage(),employeePageQueryDTO.getPageSize());

        Page<Employee> page = employeeMapper.seleteEmpByPageAndPagesize(employeePageQueryDTO);

        // 封装到PageResult中
       PageResult pageResult = new PageResult();
       pageResult.setTotal(page.getTotal());
       pageResult.setRecords(page.getResult());

        return pageResult;
    }

    /**
     * 新增员工
     * @param employeeDTO
     */
    @Override
    public void addEmp(EmployeeDTO employeeDTO) {
        // 调用emp mapper保持员工
        // 写数据库之前，准备数据，准备完之后写数据库
        // 员工表需要插入11个数据，前端传5给数据，我们需要准备6个
        log.info("[当前线程...] Thread:{}",Thread.currentThread().getId());

        Integer status = 1;
        LocalDateTime createTime = LocalDateTime.now();
        LocalDateTime updateTime = LocalDateTime.now();
        // 从token中获取ID
        Long empId = BaseContext.threadLocalEmpId.get();
        String username = BaseContext.threadLocalEmpUserName.get();
        log.info("[当前线程--] Thread:{} empId:{} username:{}",Thread.currentThread().getId(),empId,username);
        // 创建人和更新人的Id()当前登录的Id
        Long createUser = empId;
        Long updateUser = empId;

        Employee employee = new Employee();
        employee.setName(employeeDTO.getName());
        employee.setUsername(employeeDTO.getUsername());
        employee.setPhone(employeeDTO.getPhone());
        employee.setSex(employeeDTO.getSex());
        employee.setIdNumber(employeeDTO.getIdNumber());
        employee.setStatus(status);
//        employee.setCreateTime(createTime);
//        employee.setUpdateTime(updateTime);
//        employee.setCreateUser(createUser);
//        employee.setUpdateUser(updateUser);
        String md5Password = DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes());
        employee.setPassword(md5Password);
        // 调用mapper
        log.info("emp:{}",employee);
        employeeMapper.insertEmp(employee);

    }


    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();

        //1、根据用户名查询数据库中的数据
        Employee employee = employeeMapper.getByUsername(username);

        //2、处理各种异常情况（用户名不存在、密码不对、账号被锁定）
        if (employee == null) {
            //账号不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //密码比对
        // 对密码进行MD5加密
        String md5password = DigestUtils.md5DigestAsHex(password.getBytes());
        log.info("MD5加密: MD5password:{}",md5password);
        if (!md5password.equals(employee.getPassword())) {
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        if (employee.getStatus() == StatusConstant.DISABLE) {
            //账号被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }

        //3、返回实体对象
        return employee;
    }

}
