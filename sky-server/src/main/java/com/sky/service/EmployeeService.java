package com.sky.service;

import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.result.PageResult;
import com.sky.result.Result;

public interface EmployeeService {

    /**
     * 员工登录
     * @param employeeLoginDTO
     * @return
     */
    Employee login(EmployeeLoginDTO employeeLoginDTO);

    /**
     * 新增员工
     * @param employeeDTO
     */
    void addEmp(EmployeeDTO employeeDTO);

    /**
     * 分页查询员工
     * @param employeePageQueryDTO
     */
    PageResult seleteEmpByPage(EmployeePageQueryDTO employeePageQueryDTO);

    /**
     * 员工账号的启用与禁用
     * @param status
     * @param id
     */
    void startOrStop(Integer status, Long id);

    /**
     * 根据ID回显员工信息
     * @param id
     * @return
     */
    Employee getEmpById(Long id);

    /**
     * 修改员工信息
     * @param employeeDTO
     * @return
     */
    void updateEmp(EmployeeDTO employeeDTO);
}
