package com.sky.controller.user;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.ShoppingCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/shoppingCart")
@Api(tags = "购物车控制器")
@Slf4j
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @PostMapping("/sub")
    @ApiOperation("删除购物车商品")
    public Result delete(@RequestBody ShoppingCartDTO shoppingCartDTO){
        log.info("[删除购物商品] shoppingDTO:{}",shoppingCartDTO);
        shoppingCartService.deleteShoppingCart(shoppingCartDTO);
        return Result.success();
    }


    @DeleteMapping("/clean")
    @ApiOperation("清空购物车")
    public Result deleteAll(){
        log.info("[清空购物车]");
        shoppingCartService.clean();
        return Result.success();
    }

    @GetMapping("/list")
    @ApiOperation("查看购物车列表")
    public Result<List<ShoppingCart>> findShoppingCart(){
        log.info("[查看购物车列表]");
        List<ShoppingCart> list = shoppingCartService.findShoppingCart();
        return Result.success(list);
    }


    @PostMapping("/add")
    @ApiOperation("购物车接口")
    public Result addShopping(@RequestBody ShoppingCartDTO shoppingCartDTO){
        log.info("[添加购物车] shoppingDTO:{}",shoppingCartDTO);
        shoppingCartService.addShopping(shoppingCartDTO);
        return Result.success();
    }


}
