package com.sky.controller.user;

import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * 店铺控制器
 */
@RestController("UserShopControllrt")
@Api(tags = "店铺控制器")
@RequestMapping("/user/shop")
@Slf4j
public class ShopController {

    @Autowired
    private RedisTemplate redisTemplate;

    public static final String SHOP_KEY = "SHOP_STATUS";


    @GetMapping("/status")
    public Result<Integer> getStatus(){
        log.info("[获取营业状态]");

        Integer status = (Integer) redisTemplate.opsForValue().get(SHOP_KEY);
        if (status == null){
            redisTemplate.opsForValue().set(SHOP_KEY,1);
            status = 1;
        }
        return Result.success(status);
    }
}
