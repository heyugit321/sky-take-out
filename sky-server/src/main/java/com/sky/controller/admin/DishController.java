package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 菜品控制器
 */
@RestController
@Slf4j
@Api(tags = "菜品相关接口")
@RequestMapping("/admin/dish")
public class DishController {

    @Autowired
    private DishService dishService;
    @Autowired
    private RedisTemplate redisTemplate;

    @GetMapping("/list")
    @ApiOperation("根据分类ID查询菜品")
    public Result<List<Dish>> list(Long categoryId){
        log.info("[分类ID查询菜品] categoryId:{}",categoryId);
        List<Dish> dishList = dishService.getList(categoryId);
        return Result.success(dishList);
    }


    @PostMapping("/status/{status}")
    @ApiOperation("菜品的启用与禁用")
    public Result StartOrStop(@PathVariable Integer status,Long id){
        log.info("[菜品的启用与禁用] status:{} ,id:{}",status ,id);
        dishService.StartOrStop(status,id);
        return Result.success();
    }


    @PutMapping()
    @ApiOperation("更新菜品")
    public Result updateDish(@RequestBody DishDTO dishDTO){
        log.info("[更新菜品..] dishDTO:{}",dishDTO);
        dishService.updateDishById(dishDTO);
        //菜品缓存数据清理掉，所有以dish_开头的key
        String key = "dish:" + dishDTO.getCategoryId();
        cleanRedisByCategoryId(key);
        return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("回显菜品")
    public Result<DishVO> findDishById(@PathVariable Long id){
        log.info("[根据ID回显菜品] id:{}",id);
        DishVO dishVO = dishService.findDishByID(id);
        return Result.success(dishVO);
    }


    @DeleteMapping()
    @ApiOperation("批量删除菜品")   // 前端数据是 /dish?ids=10,20,30 这样的 要用RequestParam
    public Result deleteDishById(@RequestParam List<Long> ids){
        log.info("[根据ID批量删除菜品..] ids:{}" , ids);

        dishService.deleteDishAll(ids);
        //将所有的菜品缓存数据清理掉，所有以dish_开头的key
        cleanRedisByCategoryId("dish:*");

        return Result.success();
    }


    @GetMapping("/page")
    @ApiOperation("分页查询菜品")
    public Result<PageResult> getDishByPage(DishPageQueryDTO dishPageQueryDTO){
        log.info("[分页查询菜品] dishDTO:{}",dishPageQueryDTO);

        PageResult pageResult = dishService.getDishByPage(dishPageQueryDTO);
        return Result.success(pageResult);
    }

    @PostMapping()
    @ApiOperation("新增菜品")
    public Result addDish(@RequestBody DishDTO dishDTO){
        log.info("[新增菜品...] dishDTO:{}",dishDTO);

        dishService.addDish(dishDTO);
        // 清空缓存数据
        String key = "dish:"+dishDTO.getCategoryId();
        cleanRedisByCategoryId(key);

        return Result.success();
    }

    /**
     * 清空缓存公共方法
     * @param key
     */
    private void cleanRedisByCategoryId(String key) {
        Set<String> keys = redisTemplate.keys(key);
        redisTemplate.delete(keys);
    }
}
