package com.sky.controller.admin;

import com.sky.constant.JwtClaimsConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 员工管理
 */
@RestController
@RequestMapping("/admin/employee")
@Slf4j
@Api(tags = "员工相关接口")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private JwtProperties jwtProperties;


    @PutMapping()
    @ApiOperation("修改员工信息")
    public Result updateEmp(@RequestBody EmployeeDTO employeeDTO){
        log.info("[修改员工信息] empDTO:{}" , employeeDTO);
        employeeService.updateEmp(employeeDTO);
         return Result.success();
    }


    @GetMapping("/{id}")
    @ApiOperation("根据ID查询员工")
    public Result getEmpById( @PathVariable Long id){
        log.info("[根据ID查询员工] ID:{}",id);
         Employee employee = employeeService.getEmpById(id);
        return Result.success(employee);
    }


    @PostMapping("/status/{status}")
    @ApiOperation("员工账号的启用与禁用")
    public Result startOrStop ( @PathVariable Integer status, Long id){
        log.info("[启用与禁用] status:{} id:{}",status,id);

        employeeService.startOrStop(status,id);

        return Result.success();
    }


    @GetMapping("/page")
    @ApiOperation("员工分页查询")
    public Result<PageResult> seleteEmpByPage(EmployeePageQueryDTO employeePageQueryDTO){
        log.info("[分页查询员工] 参数:{}",employeePageQueryDTO);

        PageResult pageResult = employeeService.seleteEmpByPage(employeePageQueryDTO);

        return Result.success(pageResult);
    }


    @PostMapping()
    @ApiOperation("新增员工")
    public Result addEmp(@RequestBody EmployeeDTO employeeDto){
        // 打印
        log.info("[新增员工] empdto:{}",employeeDto);

        Long empId = BaseContext.threadLocalEmpId.get();
        String username = BaseContext.threadLocalEmpUserName.get();
        log.info("[当前线程] Thread:{} empId:{} username:{}",Thread.currentThread().getId(),empId,username);

        //调用
        employeeService.addEmp(employeeDto);
        //响应
        return Result.success();
    }

    /**
     * 登录
     *
     * @param employeeLoginDTO
     * @return
     */
    @PostMapping("/login")
    @ApiOperation(value = "登录接口")
    public Result<EmployeeLoginVO> login(@RequestBody EmployeeLoginDTO employeeLoginDTO) {
        log.info("员工登录：{}", employeeLoginDTO);

        Employee employee = employeeService.login(employeeLoginDTO);

        //登录成功后，生成jwt令牌
        Map<String, Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.EMP_ID, employee.getId());
        claims.put(JwtClaimsConstant.USERNAME,employee.getUsername());
        String token = JwtUtil.createJWT(
                jwtProperties.getAdminSecretKey(),
                jwtProperties.getAdminTtl(),
                claims);

        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder()
                .id(employee.getId())
                .userName(employee.getUsername())
                .name(employee.getName())
                .token(token)
                .build();

        return Result.success(employeeLoginVO);
    }

    /**
     * 退出
     *
     * @return
     */
    @PostMapping("/logout")
    @ApiOperation(value = "退出接口")
    public Result<String> logout() {
        return Result.success();
    }

}
