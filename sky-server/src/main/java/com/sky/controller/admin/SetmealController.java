package com.sky.controller.admin;

import com.github.pagehelper.Page;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/setmeal")
@Slf4j
@Api (tags = "套餐相关接口")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    @PutMapping
    @ApiOperation("更新套餐")
    @CacheEvict(cacheNames = "setmeal",key = "#setmealDTO.categoryId")
    public Result updateSetmeal(@RequestBody SetmealDTO setmealDTO){
        log.info("[更新套餐] setmeal:{}" , setmealDTO);
        setmealService.updateSetmeal(setmealDTO);
        return Result.success();
    }


    @GetMapping("/{id}")
    @ApiOperation("根据ID查询套餐")
    public Result getSetmealById(@PathVariable Long id){
        log.info("[根据ID查询套餐] id...:{}",id);
        SetmealVO setmealVO = setmealService.getSetmealById(id);
        return Result.success(setmealVO);
    }

    @DeleteMapping()
    @ApiOperation("批量删除套餐")
    @CacheEvict(cacheNames = "setmeal",allEntries = true)
    public Result deleteSetmeal( @RequestParam List<Long> ids){
        log.info("[删除套餐] id:{}",ids);
        setmealService.deleteSetmeal(ids);
        return Result.success();
    }


    @PostMapping("/status/{status}")
    @ApiOperation("套餐起售停售")
    @CacheEvict(cacheNames = "setmeal",allEntries = true)
    public Result startOrStop(@PathVariable Integer status,Long id){
        log.info("[套餐起售 停售] status:{},id:{}",status,id);
        setmealService.startOrStop(status,id);
        return Result.success();
    }


    @GetMapping("/page")
    @ApiOperation("套餐分页查询")
    public Result getSetmealByPage(SetmealPageQueryDTO pageQueryDTO){
        log.info("[套餐分页] pageDTO:{}" , pageQueryDTO);
        PageResult page = setmealService.getSetmealByPage(pageQueryDTO);
        return Result.success(page);
    }
    /**
     * 新增套餐
     * @return
     */
    @PostMapping()
    @ApiOperation("新增套餐")
    @CacheEvict(cacheNames = "setmeal",allEntries = true)
    public Result addSetmeal(@RequestBody SetmealDTO setmealDTO){
        log.info("[新增套餐..] setmealDTO:{}",setmealDTO);
        setmealService.addSetmeal(setmealDTO);
        return Result.success();
    }

}
