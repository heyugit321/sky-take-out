package com.sky.aop;

import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.beans.beancontext.BeanContext;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Component
@Aspect
@Slf4j
public class AutoFillAspect {

    @Before("@annotation(com.sky.aop.AutoFill)")
    public void autoFill(JoinPoint joinPoint){
        log.info("[自动填充....]");
        // 获得方法签名对象
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        // 获得方法上的注解
        AutoFill autoFill = signature.getMethod().getAnnotation(AutoFill.class);
        Method method = signature.getMethod();
        OperationType operationType = autoFill.value();

        Object[] arg = joinPoint.getArgs();

        if (arg == null && arg.length == 0){
            return;
        }

        Object args = arg[0];
        log.info("[自动填充...] args 反射之前:{}",args);
        if(operationType == OperationType.INSERT){
            log.info("[自定义填充..] Method:{} Insert..",method.getName());
            try {
                Method setUpdateUser = args.getClass().getDeclaredMethod("setUpdateUser", Long.class);
                Method setUpdateTime = args.getClass().getDeclaredMethod("setUpdateTime", LocalDateTime.class);
                Method setCreateUser = args.getClass().getDeclaredMethod("setCreateUser", Long.class);
                Method setCreateTime = args.getClass().getDeclaredMethod("setCreateTime", LocalDateTime.class);
                // 第一个参数 是实体类对象
                setUpdateUser.invoke(args, BaseContext.threadLocalEmpId.get());
                setUpdateTime.invoke(args,LocalDateTime.now());
                setCreateUser.invoke(args,BaseContext.getCurrentId());
                setCreateTime.invoke(args,LocalDateTime.now());
            } catch (Exception e) {
                e.printStackTrace();
                log.info("自定义填充失败:{}",e.getMessage());
            }

        }else {
            log.info("[自定义填充..] Method:{} update..",method.getName());

            try{
                Method setUpdateUser = args.getClass().getDeclaredMethod("setUpdateUser", Long.class);
                Method setUpdateTime = args.getClass().getDeclaredMethod("setUpdateTime", LocalDateTime.class);
                // 第一个参数 是实体类对象
                setUpdateUser.invoke(args, BaseContext.threadLocalEmpId.get());
                setUpdateTime.invoke(args,LocalDateTime.now());
            }catch (Exception e){
                e.printStackTrace();
                log.error("自动填充失败...:{}",e.getMessage());
            }
            log.info("[自动填充...] args 反射之后:{}",args);
        }
    }
}
