package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.aop.AutoFill;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.enumeration.OperationType;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishMapper {

    /**
     * 根据分类id查询菜品数量
     * @param categoryId
     * @return
     */
    @Select("select count(id) from dish where category_id = #{categoryId}")
    Integer countByCategoryId(Long categoryId);

    /**
     * 新增菜品
     * @param dish
     */
    @AutoFill(OperationType.INSERT)
    void insertDish(Dish dish);

    /**
     * 分页查询菜品数据
     * @param dishPageQueryDTO
     * @return
     */
    List<DishVO> selectDishByPage(DishPageQueryDTO dishPageQueryDTO);

    // 起售中的菜品不能删
    @Select("select * from dish where id = #{id}")
    Dish getDishById(Long id);
    // 套餐关联的不删
    Long getDishCount(@Param("ids") List<Long> ids);
    // 根据ID删除菜品
    @Delete("delete from dish where id = #{id}")
    void deleteDishById(Long id);

    /**
     * 菜品回显
     * @param id
     * @return
     */
    @Select("select * from dish where id = #{id}")
    Dish seleteById(Long id);

    /**
     * 更新菜品
     * @param dish
     */
    @AutoFill(OperationType.UPDATE)
    void updateDish(Dish dish);

    /**
     * 根据分类ID查询菜品
     * @param dish
     * @return
     */
    List<Dish> getList(Dish dish);

//    条件查询菜品和口味
    @Select("select * from dish where category_id = #{categoryId}")
    List<Dish> list(Dish dish);
}
