package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {

    // 查看数据库购物车有没有 数据
    List<ShoppingCart> seleteByAll(ShoppingCart selShoppingCart);

    // 插入购物车
    void insert(ShoppingCart selShoppingCart);

    //  根据这条记录的主键，去更新记录，让数量+1
    void updateById(ShoppingCart shoppingCart);

    // 清空购物车
    @Delete("delete from shopping_cart where user_id = #{userId}")
    void deleteCartByUserId(Long userId);
    //购物车单独删除
    @Update("update shopping_cart set number = #{number}  where id = #{id}")
    void updateShoppingById(ShoppingCart shoppingCart);
    // 购物车单独删除
    @Delete("delete from shopping_cart where id = #{id}")
    void deleteCartById(Long id);
}