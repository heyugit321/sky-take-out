package com.sky.mapper;

import com.sky.entity.SetmealDish;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealDishMapper {

    /**
     * 新增套餐
     * @param setmealDishes
     */
    void insertSetDish(@Param("setmealDishes") List<SetmealDish> setmealDishes);

    //删除套餐菜品关系表中的数据
    @Delete("delete from setmeal_dish where setmeal_id = #{setmealId}")
    void deleteSetmeal(Long id);

    @Select("select * from setmeal_dish where setmeal_id = #{setmeal_id}")
    List<SetmealDish> getBySetmealId(Long id);
}
