package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.aop.AutoFill;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealMapper {

    /**
     * 动态条件查询套餐
     * @param setmeal
     * @return
     */
    List<Setmeal> list(Setmeal setmeal);

    /**
     * 根据套餐id查询菜品选项
     * @param setmealId
     * @return
     */
    @Select("select sd.name, sd.copies, d.image, d.description " +
            "from setmeal_dish sd left join dish d on sd.dish_id = d.id " +
            "where sd.setmeal_id = #{setmealId}")
    List<DishItemVO> getDishItemBySetmealId(Long setmealId);



    /**
     * 根据分类id查询套餐的数量
     * @param id
     * @return
     */
    @Select("select count(id) from setmeal where category_id = #{categoryId}")
    Integer countByCategoryId(Long id);

    /**
     * 新增套餐
     * @param setmeal
     */
    @AutoFill(OperationType.INSERT)
    void insertSetmeal(Setmeal setmeal);

    /**
     * 套餐分页查询
     * @param pageQueryDTO
     * @return
     */
//    @Select("select s.*,c.name as categoryName from setmeal s join category c on c.id = s.category_id\n" +
//            "where s.name like concat('%',#{name},'%') and s.category_id = #{categoryId} and s.status = #{status}")
    Page<SetmealVO> seleteSetmeal(SetmealPageQueryDTO pageQueryDTO);

    /**
     * 起售停售
     * @param setmeal
     */
    @AutoFill(OperationType.UPDATE)
    void updateSetmeal(Setmeal setmeal);

    //
    @Select("select * from setmeal where id = #{id}")
    Setmeal getById(Long id);

    @Delete("delete from setmeal where id = #{id}")
    void deleteSetmeal(Long id);

    // 回显
    @Select("select * from setmeal where id = #{id}")
    SetmealVO getSetmealById(Long id);

    // 根据套餐ID看 是否有停售菜品
    @Select("select d.* from setmeal_dish s join dish d on s.dish_id = d.id\n" +
            "where s.setmeal_id = #{id}")
    List<Dish> getBySetmealId(Long id);
}
