package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.aop.AutoFill;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.entity.Employee;
import com.sky.enumeration.OperationType;
import com.sky.result.PageResult;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface EmployeeMapper {

    /**
     * 根据用户名查询员工
     * @param username
     * @return
     */
    @Select("select * from employee where username = #{username}")
    Employee getByUsername(String username);

    /**
     * 启用与禁用员工  修改员工信息
     * @param employee
     */
    @AutoFill(OperationType.UPDATE)
    void updateEmp(Employee employee);

    /**
     * 分页查寻员工
     * @param employeePageQueryDTO
     * @return
     */
//    @Select("select * from employee")
    Page<Employee> seleteEmpByPageAndPagesize(EmployeePageQueryDTO employeePageQueryDTO);


    /**
     * 新增员工
     * @param employee
     */
    @Insert("insert into employee\n" +
            "values (null,#{name},#{username},#{password}," +
            "#{phone},#{sex},#{idNumber},#{status},#{createTime}," +
            "#{updateTime},#{createUser},#{updateUser})")
    @AutoFill(OperationType.INSERT)
    void insertEmp(Employee employee);

    /**
     * 根据ID回显员工信息
     * @param id
     * @return
     */
    @Select("select * from employee where id = #{id}")
    Employee getEmpById(Long id);


}
